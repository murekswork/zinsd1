import csv
import re

dns_log = open('dns.log')
hosts = open('hosts.txt')
dns = csv.reader(dns_log, delimiter="\x09")
count = 0
n = 0
reg = re.compile('[^a-zA-Z.]')
bad_hosts = []

for line in hosts.readlines():
    line = re.sub(r'\[[^][]*\]', '', line)
    if line != '\n':
        bad_hosts.append(reg.sub('', line))

for row in dns:
    try:
        n += 1
        if row[9] in bad_hosts:
            print(row[9])
            count += 1
    except IndexError:
        pass

print("Процент нежелательного трафика равен "+str(count * 100.0 / n) + "%")


dns_log.close()
hosts.close()
